<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Blue Front</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
</head>

<body>
    <div class="container" id="vue-root">
        <table class="table table-striped">
            <thead class="thead-light">
                <tr>
                    <th>ID</th>
                    <th>
                        Name
                        <input id="name-value" v-on:change="reloadItems()">
                    </th>
                    <th>
                        Amount
                        <select id="amount-operator" v-on:change="reloadItems()">
                            <option></option>
                            <option>=</option>
                            <option>></option>
                            <option>>=</option>
                            <option><</option>
                            <option><=</option>
                        </select>
                        <input id="amount-value" v-on:change="reloadItems()" type="number" min="0" value="0">
                    </th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="item in items" v-bind:key="item.id">
                    <td>@{{ item.id }}</td>
                    <td>
                        <span v-if="editId !== item.id">@{{ item.name }}</span>
                        <input v-else id="edit-name" class="form-control" :value="item.name">
                    </td>
                    <td>
                        <span v-if="editId !== item.id">@{{ item.amount }}</span>
                        <input v-else id="edit-amount" class="form-control" :value="item.amount" type="number" min="0">
                    </td>
                    <td>
                        <i v-if="editId !== item.id" class="fas fa-edit btn btn-info" v-on:click="editItem(item.id)"></i>
                        <i v-else class="fas fa-save btn btn-primary" v-on:click="updateItem(item.id)"></i>
                        <i v-if="editId !== item.id" class="fas fa-trash btn btn-danger" v-on:click="deleteItem(item.id)"></i>
                        <i v-else class="fas fa-window-close btn btn-danger" v-on:click="editItem(null)"></i>
                    </td>
                </tr>
                <tr class="form-group">
                    <td></td>
                    <td>
                        <input id="add-name" class="form-control">
                    </td>
                    <td>
                        <input id="add-amount" class="form-control" type="number" min="0">
                    </td>
                    <td>
                        <button class="btn btn-success" v-on:click="addItem()">Add</button>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script>
        var apiUrl = '{{ config('blue.api_url') }}';

        function validateAmount(input) {
            let valid = input.val() !== '' && input.val() >= 0;
            if (valid) {
                input.removeClass('is-invalid');
            } else {
                input.addClass('is-invalid');
            }
            return valid;
        }

        function validateName(input) {
            let valid = input.val().trim().length >= 3;
            if (valid) {
                input.removeClass('is-invalid');
            } else {
                input.addClass('is-invalid');
            }
            return valid;
        }

        var vue = new Vue({
            el: '#vue-root',
            data: {
                editId: null,
                items: [],
            },
            methods: {
                addItem: function() {
                    var name = $('#add-name');
                    var amount = $('#add-amount');
                    let nameValid = validateName(name);
                    let amountValid = validateAmount(amount);

                    if (nameValid && amountValid) {
                        var self = this;
                        $.ajax({
                            url: apiUrl + '/item',
                            data: {
                                name: name.val(),
                                amount: amount.val()
                            },
                            method: 'POST',
                            success: function (data) {
                                self.items.push(data);
                                name.val('');
                                amount.val('');
                            },
                            error: function (error) {
                                self.items = [{id: '-', name: 'API ERROR: ' + error.responseJSON.message, amount: '-'}]
                            }
                        });
                    }
                },
                deleteItem: function(id) {
                    var self = this;
                    $.ajax({
                        url: apiUrl + '/item/' + id,
                        method: 'DELETE',
                        success: function (data) {
                            for (key in self.items) {
                                if (self.items[key].id === id) {
                                    self.items.splice(key, 1);
                                    break;
                                }
                            }
                        },
                        error: function (error) {
                            self.items = [{id: '-', name: 'API ERROR: ' + error.responseJSON.message, amount: '-'}]
                        }
                    });
                },
                editItem: function(id) {
                    this.editId = id;
                },
                updateItem: function(id) {
                    var name = $('#edit-name');
                    var amount = $('#edit-amount');
                    let nameValid = validateName(name);
                    let amountValid = validateAmount(amount);

                    if (nameValid && amountValid) {
                        var self = this;
                        $.ajax({
                            url: apiUrl + '/item/' + id,
                            method: 'POST',
                            data: {
                                name: $('#edit-name').val(),
                                amount: $('#edit-amount').val()
                            },
                            success: function (data) {
                                for (key in self.items) {
                                    if (self.items[key].id === id) {
                                        self.items[key].name = data.name;
                                        self.items[key].amount = data.amount;
                                        break;
                                    }
                                }
                                self.editId = null;
                            },
                            error: function (error) {
                                self.items = [{id: '-', name: 'API ERROR: ' + error.responseJSON.message, amount: '-'}]
                                self.editId = null;
                            }
                        });
                    }
                },
                reloadItems: function() {
                    var self = this;
                    var data = {query:{}}
                    var name = $('#name-value').val();
                    if (name.length) {
                        data.query.name = {
                            operator: 'LIKE',
                            value: name
                        }
                    }
                    var amountOperator = $('#amount-operator').val();
                    if (amountOperator) {
                        data.query.amount = {
                            operator: amountOperator,
                            value:$('#amount-value').val()
                        }
                    }
                    $.ajax({
                        url: apiUrl + '/item',
                        data: data,
                        method: 'GET',
                        success: function (data) {
                            self.items = data;
                        },
                        error: function (error) {
                            self.items = [{id: '-', name: 'API ERROR: ' + error.responseJSON.message, amount: '-'}]
                        }
                    });
                }
            },
            mounted: function() {
                this.reloadItems();
            }
        });
    </script>
</body>
</html>
