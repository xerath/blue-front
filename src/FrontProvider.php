<?php

namespace Blue\Front;

use Illuminate\Support\ServiceProvider;

class FrontProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadViewsFrom($this->getViewsPath(), 'blue');
        $this->mergeConfigFrom($this->getConfigPath(), 'blue');

        if ($this->app->runningInConsole()) {
            $this->registerPublishableResources();
        }
	}

    /**
     * Register the publishable files.
     */
    private function registerPublishableResources(): void
    {
        $publishable = [
            'config' => [
                $this->getConfigPath() => config_path('blue.php'),
            ],
            'views' => [
                $this->getViewsPath() => resource_path('views/vendor/blue'),
            ],
        ];

        foreach ($publishable as $group => $paths) {
            $this->publishes($paths, $group);
        }
    }

    private function getPublishablePath(): string
    {
        return dirname(__DIR__) . '/publishable/';
    }

    private function getConfigPath(): string
    {
        return "{$this->getPublishablePath()}config/blue.php";
    }

    private function getViewsPath(): string
    {
        return "{$this->getPublishablePath()}resources/views/";
    }
}
