# Blue Services front
This is a laravel package providing front solution to the homework.

The view is available on the `/` url.

## Install
Add the repo to your `composer.json` file

```
{
    "repositories": [
        {
            "url": "https://bitbucket.org/xerath/blue-front.git",
            "type": "git"
        }
    ],
}
```

Run the command

```
composer require blue/front
```

## Configuration
In the `.env` file you need to configure `BLUE_API_URL` value

## Publishing vendor files

### Config
```
 php artisan vendor:publish --provider=Blue\\Front\\FrontProvider --tag=config
```

### Views
```
 php artisan vendor:publish --provider=Blue\\Front\\FrontProvider --tag=views
```
